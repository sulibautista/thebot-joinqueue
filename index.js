"use strict";

const
  uuidGen = require('node-uuid');
  
let redis;
  
let joinQueue = {
  joinQueueKey: 'channelJoin:queue',
  joinNotificationsKey: 'channelJoin:notifications',
  joinStatusKey: 'channelJoin:status',
  channelHandlersKey: 'channelHandler',
  channelHandlerStorageKey: 'channelHandler:{uuid}',
  channelMessagesKey: 'channelMessages:{channel}',
};

module.exports = function(_redis){
  if(!_redis)
    throw new Error('called without redis instance');
    
  redis = _redis;
  return joinQueue;
};

let redisCmd = {};

/**
 * Adds to the queue only if not there already.
 * @param channel
 * @param priority
 */
redisCmd.enqueueJoin = 
"if not redis.call('ZSCORE', KEYS[1], ARGV[1]) then \
  redis.call('ZADD', KEYS[1], ARGV[2], ARGV[1]) \
  return redis.call('PUBLISH', KEYS[2], 1)\
end \
return nil \
";

redisCmd.keepHandlerAlive = 
"if redis.call('ZSCORE', KEYS[1], ARGV[1]) then \
  return redis.call('ZADD', KEYS[1], ARGV[2], ARGV[1]) \
else \
  return redis.error_reply('BOT_MISSING_HANDLER') \
end \
";

redisCmd.dequeueJoin = 
"if not redis.call('ZSCORE', KEYS[1], ARGV[1]) then \
  return redis.error_reply('BOT_MISSING_HANDLER'); \
end \
local channel = redis.call('ZRANGE', KEYS[2], 0, 0) \
if channel[1] then \
  channel = channel[1] \
  redis.call('ZREM', KEYS[2], channel) \
  local status = redis.call('HGET', KEYS[3], channel) \
  if not status then \
    redis.call('SADD', KEYS[4], channel) \
    redis.call('HSET', KEYS[3], channel, 'joining') \
    return channel \
  end \
end \
return nil \
";


joinQueue.getJoinQueueKey = function(){
  return joinQueue.joinQueueKey;
}

joinQueue.getJoinNotificationsKey = function(){
  return joinQueue.joinNotificationsKey;
}

joinQueue.getJoinStatusKey = function(){
  return joinQueue.joinStatusKey;
}

joinQueue.getChannelHandlersKey = function(){
  return joinQueue.channelHandlersKey;
}

joinQueue.getChannelHandlerStorageKey = function(uuid){
  return joinQueue.channelHandlerStorageKey.replace('{uuid}', uuid);
}

joinQueue.getChannelMessagesKey = function(channel){
  return joinQueue.channelMessagesKey.replace('{channel}', channel);
}

/**
 * Enqueues the channel to be joined.
 * The given callback is passed either an integer or a null, if its null, the channel was already in the queue. 
 * If an integer, it indicates how many clients are listening to the queue.
 * @param {string} chId The ID of the channel
 * @param {object } options Object with keys: 
 *  priority - the join priority
 *  credentials - object containing special account information to use for the join
 * @param {function} callback 
 */
joinQueue.enqueueJoin = function(chId, priority, callback){
  redis.eval(
    redisCmd.enqueueJoin, 
    2, joinQueue.getJoinQueueKey(), joinQueue.getJoinNotificationsKey(),
    chId,
    Date.now(),
  callback);
}

joinQueue.registerHandler = function(callback){
  let uuid = uuidGen.v1();
  redis.zadd(joinQueue.getChannelHandlersKey(), Date.now(), uuid, function(err){
    if(err) return callback(err);
    callback(null, uuid);
  });
}

joinQueue.keepHandlerAlive = function(uuid, callback){
  redis.eval(
    redisCmd.keepHandlerAlive,
    1, joinQueue.getChannelHandlersKey(),
    uuid,
    Date.now(),    
  callback);
}

/**
 * Dequeues a channel (if not empty) from the join queue. 
 * If a channel is successfully removed from the queue and verified offline, it is added to a channel handler's uuid key.
 */
joinQueue.dequeueJoin = function(uuid, callback){
  redis.eval(
    redisCmd.dequeueJoin,
    4, 
      joinQueue.getChannelHandlersKey(),
      joinQueue.getJoinQueueKey(),
      joinQueue.getJoinStatusKey(),
      joinQueue.getChannelHandlerStorageKey(uuid),
    uuid,
    callback
  );
}

joinQueue.dropJoin = function(ch, callback){
  redis.zrem(joinQueue.getJoinQueueKey(), ch, callback);
}

joinQueue.getJoinStatus = function(ch, callback){
  redis.hget(joinQueue.getJoinStatusKey(), ch, callback);
}

joinQueue.setJoinStatus = function(ch, status, callback){
  redis.hset(joinQueue.getJoinStatusKey(), ch, status, callback);
}

joinQueue.delJoinStatus = function(ch, callback){
  redis.hdel(joinQueue.getJoinStatusKey(), ch, callback);
}